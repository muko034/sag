package elka;

import elka.lib.Lib;
import elka.server.Server;

public class Main {
    public static final int SERVER_PORT = 2551;
    public static final String SERVER_HOST = "localhost";
    public static final String LIB_ENDPOINT = "/user/representative";
    public static final String SERVER_ENDPOINT = "/user/manager";


    public static void main(String[] args) {

        if (args.length == 0) {
            help();
        } else if (args.length == 1) {
            switch (args[0]) {
                case "server":
                    Server.run();
                    break;
                case "lib":
                    Lib.run("lib0");  // TODO: pobieranie nazwy z parametrow programu
                    break;
                case "help":
                    help();
                    break;
                default:
                    System.out.println("unknown parameter");
                    help();
                    break;
            }
        } else {
            System.out.println("Wrong parameters number");
            help();
        }
    }

    private static void help() {
        System.out.println("TODO help message");
    }
}
