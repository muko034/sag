package elka.lib.agent;

import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import akka.pattern.Patterns;
import akka.util.Timeout;
import elka.common.DocumentClassifier;
import elka.common.agent.DocClassifierHandler;
import elka.common.message.DocClassifierMessage;
import elka.lib.Lib;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;


import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static elka.Main.LIB_ENDPOINT;
import static elka.common.message.SimpleMessage.MANAGER_REGISTRATION;

public class Representative extends UntypedActor {

    private Lib library;
    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    private Cluster cluster = Cluster.get(getContext().system());
    private Optional<ActorRef> manager = Optional.empty();
    private Optional<ActorRef> docClassifierHandler = Optional.empty();

    public Representative(Lib library) {
        this.library = library;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        log.info("Get message: \"{}\" from: {}", message.toString(), getSender());

        if (message instanceof String) {
            String text = (String) message;

            switch (text) {
                case MANAGER_REGISTRATION:
                    manager = Optional.of(this.getSender());

                    // FIXME inny agent powinien wysyłać przez representative NEED_HIM
                    getContext().system().scheduler().scheduleOnce(Duration.create(3, TimeUnit.SECONDS),
                            getSelf(), DocClassifierMessage.NEED_HIM, getContext().system().dispatcher(), getSelf());

                    break;
                case DocClassifierMessage.NEED_HIM:
                case DocClassifierMessage.IS_FREE:

                    tryForwardToManager(message);
                    break;
                default:
                    unhandled(message);
            }
        } else if (message instanceof DocClassifierMessage.IsHere) {
            tryForwardToManager(message);
        } else if (message instanceof DocClassifierMessage.SendToRequest) {
            Address address = ((DocClassifierMessage.SendToRequest) message).address;

            Timeout timeout = new Timeout(Duration.create(5, "seconds"));

            Future<Object> future = Patterns.ask(docClassifierHandler.get(),    // FIXME docClassifierHandler.ifPresent()
                    new DocClassifierMessage.SendToRequest(null), timeout);

            DocumentClassifier result = ((DocClassifierMessage.WithHim) Await.result(future, timeout.duration())).docClassifier;

            getContext().actorSelection(address + LIB_ENDPOINT)
                    .tell(new DocClassifierMessage.WithHim(result), getSelf());

            docClassifierHandler = Optional.empty();

        } else if (message instanceof DocClassifierMessage.WithHim) {

            DocumentClassifier docClassifier =  ((DocClassifierMessage.WithHim) message).docClassifier;
            docClassifierHandler = Optional.of(getContext().system().actorOf(
                    DocClassifierHandler.props(docClassifier, getSelf(), library), "docClassifierHandler"));

            manager.get().tell(new DocClassifierMessage.IsHere(cluster.selfAddress()), getSelf());
            docClassifierHandler.get().tell(DocClassifierMessage.CLASSIFY, getSelf());
        } else {
            unhandled(message);
        }
    }

    private void tryForwardToManager(Object message) {
        manager.ifPresent(m -> m.forward(message, getContext()));
    }

    public static Props props(final Lib library) {
        return Props.create(new Creator<Representative>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Representative create() throws Exception {
                return new Representative(library);
            }
        });
    }
}
