package elka.lib;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import elka.common.DocumentScanner;
import elka.lib.agent.Representative;
import elka.common.Document;
import elka.common.Consts;
import elka.common.Logger;

import java.io.File;
import java.util.*;

/**
 * Klasa reprezentujaca biblioteke - zbior dokumentow i system aktorow znajdujacych sie
 * w jej obrebie.
 */
public class Lib {
    private boolean finish = false;
    private ActorRef representative;

    private String name;
    private Map<String, Document> documentsByName;
    private ArrayList<Document> documents;

    public String getName() { return name; }
    public int getDocumentsNum() {
        return documents.size();
    }
    public Document getDocument(int index) {
        return documents.get(index);
    }
    public Document getDocument(String fileName) {
        return documentsByName.get(fileName);
    }

    public Lib(String name) {
        this.name = name;
        documentsByName = new TreeMap<>();
        documents = new ArrayList<>();
    }

    public static void run(String name) {
        Lib lib = new Lib(name);
        lib.loadLibrary();
        lib.start();
    }

    public void loadLibrary() {

        System.out.println("Importing library books...");
        DocumentScanner scanner = new DocumentScanner();
        DocumentScanner.Entry[] documents = scanner.walk();

        // create random books set
        final int booksInLibrary = 10;
        DocumentScanner.shuffleArray(documents, new Random().nextInt());
        DocumentScanner.Entry[] learningDocs = Arrays.copyOfRange(documents, 0, booksInLibrary);

        for (DocumentScanner.Entry entry : learningDocs) {
            System.out.println("Importing '" + entry.name + "' (" + entry.label + ")...");
            Document doc = new Document(entry.label + File.separator + entry.name, entry.name);
            this.documents.add(doc);
            this.documentsByName.put(entry.name, doc);
        }
    }

    private void start() {
        Logger.info("Lib started.");

        final Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=0").
                withFallback(ConfigFactory.parseString("akka.cluster.roles = [lib]")).
                withFallback(ConfigFactory.load());

        ActorSystem system = ActorSystem.create("ClusterSystem", config);

        representative = system.actorOf(Props.create(Representative.class, this), "representative");
    }
}
