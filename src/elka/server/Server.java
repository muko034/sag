package elka.server;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import elka.Main;
import elka.common.Logger;
import elka.common.DocumentClassifier;
import elka.server.agent.Manager;
import elka.common.agent.SimpleClusterListener;
import elka.common.message.DocClassifierMessage;

public class Server {

    private ActorRef clusterListener;
    private ActorRef manager;
    private ActorRef docClassifierHandler;

    public Server() {
    }

    public static void run() {
        Server server = new Server();
        server.start();
    }

    private void start() {
        Logger.info("Server started.");

        final Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + Integer.toString(Main.SERVER_PORT)).
                withFallback(ConfigFactory.parseString("akka.cluster.roles = [server]")).
                withFallback(ConfigFactory.load());

        ActorSystem system = ActorSystem.create("ClusterSystem", config);

        clusterListener = system.actorOf(Props.create(SimpleClusterListener.class), "clusterListener");
        manager = system.actorOf(Props.create(Manager.class), "manager");

        manager.tell(new DocClassifierMessage.WithHim(new DocumentClassifier()), manager);

        Logger.info("Server shutting down.");
    }
}
