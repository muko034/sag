package elka.server.agent;

import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.pattern.Patterns;
import akka.util.Timeout;
import elka.common.DocumentClassifier;
import elka.common.agent.DocClassifierHandler;
import elka.common.message.DocClassifierMessage;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import static elka.Main.*;
import static elka.common.message.SimpleMessage.*;

import java.util.*;

public class Manager extends UntypedActor {

    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    private Cluster cluster = Cluster.get(getContext().system());
    private Queue<Address> libsAddresses = new LinkedList<>();
    private Optional<Address> docClassifierPlace = Optional.empty();
    private boolean isDocClassifierFree = true;
    private Optional<ActorRef> docClassifierHandler = Optional.empty();

    //subscribe to cluster changes, MemberUp
    @Override
    public void preStart() {
        cluster.subscribe(getSelf(), ClusterEvent.MemberUp.class);
    }

    //re-subscribe when restart
    @Override
    public void postStop() {
        cluster.unsubscribe(getSelf());
    }

    @Override
    public void onReceive(Object message) throws Exception {
        log.info("Get message: \"{}\" from: {}", message.toString(), getSender());

        if (message instanceof ClusterEvent.CurrentClusterState) {
            ClusterEvent.CurrentClusterState state = (ClusterEvent.CurrentClusterState) message;
            for (Member member : state.getMembers()) {
                if (member.status().equals(MemberStatus.up())) {
                    register(member);
                }
            }
        } else if (message instanceof ClusterEvent.MemberUp) {
            ClusterEvent.MemberUp mUp = (ClusterEvent.MemberUp) message;
            register(mUp.member());
        } else if (message instanceof String) {
            String text = (String) message;

            switch(text) {
                case DocClassifierMessage.NEED_HIM: {
                    Address fromAddr = getSender().path().address();

                    if (!libsAddresses.contains(fromAddr)) {
                        libsAddresses.add(fromAddr);
                    }

                    sendDocClassifierSendToRequest();
                    break;
                }
                case DocClassifierMessage.IS_FREE: {
                    isDocClassifierFree = true;

                    sendDocClassifierSendToRequest();
                    break;
                }
                default:
                    unhandled(message);
            }

        } else if (message instanceof DocClassifierMessage.IsHere) {

            Address address = ((DocClassifierMessage.IsHere) message).address;
            docClassifierPlace = Optional.of(address);

        } else if (message instanceof DocClassifierMessage.SendToRequest) {

            Address address = ((DocClassifierMessage.SendToRequest) message).address;

            Timeout timeout = new Timeout(Duration.create(5, "seconds"));

            Future<Object> future = Patterns.ask(docClassifierHandler.get(),    // FIXME docClassifierHandler.ifPresent()
                    new DocClassifierMessage.SendToRequest(null), timeout);

            DocumentClassifier result = ((DocClassifierMessage.WithHim) Await.result(future, timeout.duration())).docClassifier;

            getContext().actorSelection(address + LIB_ENDPOINT)
                    .tell(new DocClassifierMessage.WithHim(result), getSelf());

            docClassifierHandler = Optional.empty();
            docClassifierPlace = Optional.empty();
            isDocClassifierFree = false;

        } else if (message instanceof DocClassifierMessage.WithHim) {

            DocumentClassifier docClassifier =  ((DocClassifierMessage.WithHim) message).docClassifier;

            docClassifierHandler = Optional.of(getContext().system().actorOf(
                    DocClassifierHandler.props(docClassifier, getSelf(), null), "docClassifierHandler"));
            docClassifierPlace = Optional.of(cluster.selfAddress());
            isDocClassifierFree = false;

        } else {
            unhandled(message);
        }
    }

    private void register(Member member) {
        if (member.hasRole("lib")) {
            getContext().actorSelection(member.address() + LIB_ENDPOINT)
                    .tell(MANAGER_REGISTRATION, getSelf());
        }
    }

    private void sendDocClassifierSendToRequest() {
        if ((!libsAddresses.isEmpty()) && docClassifierPlace.isPresent() && isDocClassifierFree) {
            Address fromAddr = docClassifierPlace.get();
            Address toAddr = libsAddresses.poll();
            isDocClassifierFree = false;
            docClassifierPlace = Optional.empty();
            String endPoint = isItMyAddress(fromAddr) ? SERVER_ENDPOINT : LIB_ENDPOINT;

            getContext().actorSelection(fromAddr + endPoint)
                    .tell(new DocClassifierMessage.SendToRequest(toAddr), getSelf());
        }
    }

    private boolean isItMyAddress(Address addr) {
        return cluster.selfAddress().equals(addr);
    }

}
