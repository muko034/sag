package elka.common;

import java.io.File;
import java.io.FileFilter;

class DataFileFilter implements FileFilter {
    public boolean accept(File file) {
        return file.toString().endsWith(".txt");
    }
}