package elka.common;

import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.FileIterator;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class ClassifierData {

    private InstanceList instances;

    public static Pipe buildImportPipe() {
        ArrayList pipeList = new ArrayList();

        // Read data from File objects
        pipeList.add(new Input2CharSequence("UTF-8"));

        // Regular expression for what constitutes a token.
        //  This pattern includes Unicode letters, Unicode numbers,
        //   and the underscore character. Alternatives:
        //    "\\S+"   (anything not whitespace)
        //    "\\w+"    ( A-Z, a-z, 0-9, _ )
        //    "[\\p{L}\\p{N}_]+|[\\p{P}]+"   (a group of only letters and numbers OR
        //                                    a group of only punctuation marks)
        Pattern tokenPattern =
                Pattern.compile("[\\p{L}\\p{N}_]+");

        // Tokenize raw strings
        pipeList.add(new CharSequence2TokenSequence(tokenPattern));

        // Normalize all tokens to all lowercase
        pipeList.add(new TokenSequenceLowercase());

        // Remove stopwords from a standard English stoplist.
        pipeList.add(new TokenSequenceRemoveStopwords(false, false));

        // Rather than storing tokens as strings, convert them to integers by looking them up in an alphabet.
        pipeList.add(new TokenSequence2FeatureSequence());

        // Do the same thing for the "target" field:
        // convert a class label string to a Label object,
        // which has an index in a Label alphabet.
        pipeList.add(new Target2Label());

        // Now convert the sequence of features to a sparse vector,
        // mapping feature IDs to counts.
        pipeList.add(new FeatureSequence2FeatureVector());

        return new SerialPipes(pipeList);
    }

    private static Pipe importPipe = buildImportPipe();

    public ClassifierData()
    {
        instances = new InstanceList(importPipe);
    }

    InstanceList getInstances() {
        return instances;
    }

    public void addDirectory(File directory) {
        addDirectories(new File[] {directory});
    }

    public void addDirectories(File[] directories) {

        FileIterator iterator =
                new FileIterator(directories, new DataFileFilter(), FileIterator.LAST_DIRECTORY);
        instances.addThruPipe(iterator);
    }

    public void addString(String name, String label, String data) {
        Instance inst = new Instance(data, label, name, null);
        instances.addThruPipe(inst);
    }

    public void saveToFile(File file) {
        instances.save(file);
    }
}
