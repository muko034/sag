package elka.common;

public class Consts {
    public final static String librariesLocation = "data/libraries/";
    public final static String documentsLocation = "data/documents/";
}
