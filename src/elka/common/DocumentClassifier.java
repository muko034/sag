package elka.common;

import java.io.*;
import java.net.URI;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import cc.mallet.classify.*;
import cc.mallet.pipe.iterator.*;
import cc.mallet.types.*;

public class DocumentClassifier implements Serializable {

    public enum State {
        UNTRAINED,
        TRAINED
    }

    private State state;
    private Classifier classifier;
    private Map<String, String> classifiedDocuments; // name -> label

    public DocumentClassifier()
    {
        this.state = State.UNTRAINED;
        classifiedDocuments = new TreeMap<>();
    }

    public State getState() {
        return state;
    }

    private void setState(State state) {
        this.state = state;
    }

    /**
     * Train the classifier with new data.
     * @param learningData Learning data.
     */
    public void train(ClassifierData learningData) {
        // TODO: nie wiadomo czemu, ale MaxEnt trainer nie dziala przy "douczaniu"
        NaiveBayes oldClassifier = (NaiveBayes) classifier;
        NaiveBayesTrainer trainer = new NaiveBayesTrainer(oldClassifier);
        classifier = trainer.train(learningData.getInstances());

        setState(State.TRAINED);
    }

    /**
     * Load the classifier from file.
     * @param serializedFile Source file.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void loadFromFile(File serializedFile)
            throws IOException, ClassNotFoundException {

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(serializedFile));
        classifier = (Classifier)ois.readObject();
        ois.close();
    }

    /**
     * Save the classifier to file.
     * @param serializedFile Target file.
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void saveToFile(File serializedFile)
            throws IOException {

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream (serializedFile));
        oos.writeObject (classifier);
        oos.close();
    }

    /**
     * Classify a document in a string.
     * @param str String with a document.
     * @return Most probable label
     */
    public String classifyString(String name, String str) {

        if (classifiedDocuments.containsKey(name))
        {
            String label = classifiedDocuments.get(name);
            System.out.println(name + " already classified, label = " + label);
            return label;
        }

        Instance instance = new Instance(str, null, null, null);
        instance = classifier.getInstancePipe().instanceFrom(instance);
        Classification classification = classifier.classify(instance);

        Labeling labeling = classification.getLabeling();

        // print the labels with their weights in descending order (ie best first)
        for (int rank = 0; rank < labeling.numLocations(); rank++) {
            Label label = labeling.getLabelAtRank(rank);
            String valueStr = String.format(Locale.ENGLISH, "%.3f", labeling.getValueAtRank(rank));
            System.out.print(label + ": " + valueStr + ", ");
        }

        System.out.println();
        String label = labeling.getLabelAtRank(0).toString();
        classifiedDocuments.put(name, label);
        return label;
    }

    /**
     * Classify all documents in a directory
     * @param directoryFile Directory handle with TXT files to classify.
     * @throws IOException
     */
    public void classifyDirectory(File directoryFile)
            throws IOException {

        FileIterator reader =
                new FileIterator(directoryFile, new DataFileFilter(), FileIterator.LAST_DIRECTORY);
        Iterator instances = classifier.getInstancePipe().newIteratorFrom(reader);

        while (instances.hasNext()) {
            Instance instance = (Instance)instances.next();
            URI name = (URI)instance.getName();
            System.out.print(name.toString() + "\n");
            Classification classification = classifier.classify(instance);
            Labeling labeling = classification.getLabeling();

            // print the labels with their weights in descending order (ie best first)
            for (int rank = 0; rank < labeling.numLocations(); rank++) {
                Label label = labeling.getLabelAtRank(rank);
                String valueStr = String.format(Locale.ENGLISH, "%.3f", labeling.getValueAtRank(rank));
                System.out.print(label + ": " + valueStr + ", ");
            }

            System.out.println();
        }
    }

    public void printHistory()
    {
        System.out.println("Classification history so far:");
        if (classifiedDocuments.size() == 0)
            System.out.println("(empty)");
        else {
            for (Map.Entry<String, String> entry : classifiedDocuments.entrySet()) {
                System.out.println(entry.getKey() + " - " + entry.getValue());
            }
        }
    }

    // test
    public static void main (String[] args)
            throws IOException {

        DocumentClassifier classifier = new DocumentClassifier();
        ClassifierData learningData = new ClassifierData();

        System.out.println("Importing training data:");
        DocumentScanner scanner = new DocumentScanner();
        DocumentScanner.Entry[] documents = scanner.walk();

        // create random learning and testing data sets
        DocumentScanner.shuffleArray(documents, 0);
        DocumentScanner.Entry[] learningDocs = Arrays.copyOfRange(documents, 0, documents.length / 2);
        DocumentScanner.Entry[] testDocs = Arrays.copyOfRange(documents, documents.length /2 + 1, documents.length - 1);

        for (DocumentScanner.Entry entry : learningDocs) {
            System.out.println("Importing '" + entry.name + "' (" + entry.label + ")...");
            Document doc = new Document(entry.label + File.separator + entry.name, entry.name);
            learningData.addString(entry.name, entry.label, doc.getContent());
        }

        System.out.println("Training classifier...");
        classifier.train(learningData);

        System.out.println("\nCLASSIFICATION");
        int correct = 0;
        for (DocumentScanner.Entry entry : testDocs) {
            System.out.println("Classifying '" + entry.name + "' (should be '" + entry.label + "')...");
            Document doc = new Document(entry.label + File.separator + entry.name, entry.name);
            String probLabel = classifier.classifyString(entry.name, doc.getContent());
            if (Objects.equals(entry.label, probLabel))
                correct++;
        }

        double correctPercent = 100.0 * (double)correct / (double)testDocs.length;
        System.out.print("\nCorrectly classified: " + correct + " of " + testDocs.length);
        System.out.println(String.format(Locale.ENGLISH, "(%.1f%%)", correctPercent));
    }
}
