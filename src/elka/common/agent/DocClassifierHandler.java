package elka.common.agent;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import elka.common.ClassifierData;
import elka.common.Document;
import elka.common.DocumentClassifier;
import elka.common.DocumentScanner;
import elka.common.message.DocClassifierMessage;
import elka.lib.Lib;
import scala.concurrent.duration.Duration;

import java.io.File;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class DocClassifierHandler extends UntypedActor {

    private Lib library;
    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    private DocumentClassifier docClassifier;
    private ActorRef dispatcher;

    public DocClassifierHandler(DocumentClassifier docClassifier, ActorRef ref, Lib library) {
        this.docClassifier = docClassifier;
        this.dispatcher = ref;
        this.library = library;
    }

    @Override
    public void preStart() {
        log.info("Starting...");
        if (docClassifier.getState().equals(DocumentClassifier.State.UNTRAINED)) {

            System.out.println("Importing training data...");
            DocumentScanner scanner = new DocumentScanner();
            DocumentScanner.Entry[] documents = scanner.walk();

            // create random books set
            final int learningSetSize = 20;
            DocumentScanner.shuffleArray(documents, new Random().nextInt());
            DocumentScanner.Entry[] learningDocs = Arrays.copyOfRange(documents, 0, learningSetSize);

            ClassifierData learningData = new ClassifierData();
            for (DocumentScanner.Entry entry : learningDocs) {
                System.out.println("Importing '" + entry.name + "' (" + entry.label + ")...");
                Document doc = new Document(entry.label + File.separator + entry.name, entry.name);
                learningData.addString(entry.name, entry.label, doc.getContent());
            }

            System.out.println("Training classifier...");
            docClassifier.train(learningData);
            System.out.println("Trained");

            getContext().system().scheduler().scheduleOnce(Duration.create(1, TimeUnit.SECONDS),
                    dispatcher, DocClassifierMessage.IS_FREE, getContext().system().dispatcher(), getSelf());
        }
    }

    @Override
    public void postStop() {
        log.info("Stopped");
    }

    @Override
    public void onReceive(Object message) throws Exception {
        log.info("Get message: \"{}\" from: {}", message.toString(), getSender());

        if (message instanceof String) {
            String text = (String) message;

            switch (text) {
                case DocClassifierMessage.CLASSIFY:
                    System.out.println("Classifying...");
                    if (library != null)
                    {
                        for (int i = 0; i < library.getDocumentsNum(); i++)
                        {
                            Document doc = library.getDocument(i);
                            System.out.println("Classifying " + doc.getName() + "...");
                            docClassifier.classifyString(doc.getName(), doc.getContent());
                        }
                    }

                    docClassifier.printHistory();

                    // FIXME usunac opoznienie
                    getContext().system().scheduler().scheduleOnce(Duration.create(1, TimeUnit.SECONDS),
                            dispatcher, DocClassifierMessage.IS_FREE, getContext().system().dispatcher(), getSelf());
                    break;
                default:
                    unhandled(message);
            }
        } else if (message instanceof DocClassifierMessage.SendToRequest) {
            getSender().tell(new DocClassifierMessage.WithHim(docClassifier), getSelf());
            getContext().stop(getSelf());
        } else {
            unhandled(message);
        }

    }

    public static Props props(final DocumentClassifier other, final ActorRef ref, final Lib library) {
        return Props.create(new Creator<DocClassifierHandler>() {
            private static final long serialVersionUID = 1L;

            @Override
            public DocClassifierHandler create() throws Exception {
                return new DocClassifierHandler(other, ref, library);
            }
        });
    }

}
