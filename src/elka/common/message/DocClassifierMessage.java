package elka.common.message;

import akka.actor.Address;
import elka.common.DocumentClassifier;

public class DocClassifierMessage implements IMessage {

    public static final String NEED_HIM = "I need the Documents Classifier";
    public static final String IS_FREE = "The Documents Classifier is free";
    public static final String CLASSIFY = "Classify";

    public static class SendToRequest implements IMessage {

        public final Address address;

        public SendToRequest(Address address) {
            this.address = address;
        }

        @Override
        public String toString() {
            return "DocClassifierMessage.SendToRequest { " +
                    "address: " + this.address +
                    " }";
        }
    }

    public static class WithHim implements IMessage {

        public final DocumentClassifier docClassifier;

        public WithHim(DocumentClassifier docClassifier) {
            this.docClassifier = docClassifier;
        }

        @Override
        public String toString() {
            return "DocClassifierMessage.WithHim";
        }
    }

    public static class IsHere implements IMessage {

        public final Address address;

        public IsHere(Address address) {
            this.address = address;
        }

        @Override
        public String toString() {
            return "DocClassifierMessage.IsHere { " +
                    "address: " + this.address +
                    " }";
        }
    }
}
