package elka.common.message;

public class SimpleMessage implements IMessage {

    public static final String MANAGER_REGISTRATION = "MANAGER_REGISTRATION";

    public final String text;

    public SimpleMessage() {
        this.text = "";
    }

    public SimpleMessage(String text) {
        this.text = text;
    }
}
