package elka.common;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Klasa reprezentujaca jeden dokument w bibliotece.
 */
public class Document {
    private String fileName;
    private String name;
    private String content;

    public String getContent() { return content; }
    public String getFileName() { return fileName; }
    public String getName() { return name; }

    public Document(String fileName, String name) {
        this.fileName = fileName;
        this.name = name;

        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0)
            extension = fileName.substring(i+1);
        extension = extension.toLowerCase();


        switch (extension) {
            case "txt":
                readTxtFile();
                break;
            case "pdf":
                readPdfFile();
                break;
            default:
                Logger.error("unsupported extension");
        }
    }

    private boolean readTxtFile()
    {
        try {
            content = new String(Files.readAllBytes(Paths.get(Consts.documentsLocation + fileName)));
        } catch (IOException e) {
            Logger.error("can not open document '" + fileName + "'");
            return false;
        }
        return true;
    }

    private boolean readPdfFile()
    {
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        PDFTextStripper pdfStripper;

        File file = new File(Consts.documentsLocation + fileName);
        try {
            PDFParser parser = new PDFParser(new FileInputStream(file));
            parser.parse();
            cosDoc = parser.getDocument();
            pdfStripper = new PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);
            content = pdfStripper.getText(pdDoc);
            pdDoc.close();

        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (cosDoc != null)
                    cosDoc.close();
                if (pdDoc != null)
                    pdDoc.close();
            } catch (Exception e1) {
                e.printStackTrace();
            }
            return false;
        }

        return true;
    }
}
