package elka.common;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Helper class for scanning for documents.
 */
public class DocumentScanner {

    public static final String DOCUMENTS_LOCATION = "data/documents";

    public class Entry {
        public String label;
        public String name;
        public String path;
    }

    public static void shuffleArray(Entry[] ar, long seed)
    {
        Random rnd = new Random(seed);
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            Entry a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    /**
     * Walk single directory (label)
     * @param dirFile
     * @return List of filenames in the directory
     */
    private String[] walkLabel(File dirFile) {

        ArrayList<String> result = new ArrayList<>();

        File[] list = dirFile.listFiles();
        if (list == null)
            return result.toArray(new String [result.size()]);

        for (File f : list)
            if (!f.isDirectory())
                result.add(f.getName());

        return result.toArray(new String [result.size()]);
    }

    /**
     * Walk throught documents directory.
     * @return Full list containing document name, label and path.
     */
    public Entry[] walk() {

        ArrayList<Entry> result = new ArrayList<>();
        File root = new File(DOCUMENTS_LOCATION);
        File[] list = root.listFiles();
        if (list == null)
            return result.toArray(new Entry [result.size()]);

        for (File f : list) {
            if (f.isDirectory()) {
                String[] files = walkLabel(f);
                for (String file : files) {
                    Entry entry = new Entry();
                    entry.label = f.getName();
                    entry.name = file;
                    entry.path = f.getAbsolutePath() + File.separator + file;
                    result.add(entry);
                }
            }
        }

        return result.toArray(new Entry [result.size()]);
    }

    public static void main(String[] args) {
        DocumentScanner scanner = new DocumentScanner();
        Entry[] documents = scanner.walk();

        for (Entry doc : documents) {
            System.out.println(doc.label + ": " + doc.name + " (" + doc.path + ")");
        }
    }

}
